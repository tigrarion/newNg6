import angular from 'angular';
import uiRouter from 'angular-ui-router';
import stasComponent from './stas.component';


let stasModule = angular.module('stas', [
  uiRouter
])

.config(($stateProvider) => {
  "ngInject";
  $stateProvider
    .state('stas', {
      url: '/stas',
      component: 'stas'
    });
})

.component('stas', stasComponent)
  
.name;

export default stasModule;