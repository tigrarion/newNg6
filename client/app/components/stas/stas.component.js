import template from './stas.html';
import controller from './stas.controller';
/*import './stas.data.json';*/
import './stas.css';
import './bootstrap/css/bootstrap.css';




let stasComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default stasComponent;